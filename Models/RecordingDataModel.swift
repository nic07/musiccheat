//
//  RecordingDataModel.swift
//  MusicCheat
//
//  Created by Katutu on 11/14/19.
//  Copyright © 2019 Fulsto Engineering. All rights reserved.
//

import SwiftUI

struct Recording {
    let fileURL: URL
    let createdAt: Date
}
