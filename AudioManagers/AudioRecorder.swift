//
//  AudioRecorder.swift
//  MusicCheat
//
//  Created by Katutu on 11/14/19.
//  Copyright © 2019 Fulsto Engineering. All rights reserved.
//

import SwiftUI
import AVFoundation
import Combine

class AudioRecorder: NSObject, ObservableObject {
    let objectWillChange = PassthroughSubject<AudioRecorder, Never>()
    
    var audioRecorder: AVAudioRecorder!
    var recordings = [Recording]()
    var recording = false {
        didSet {
            objectWillChange.send(self)
        }
    }
    
    override init() {
        super.init()
        fetchRecordings()
    }
    
    func startRecording(paused: Bool) {
        let recordingSession = AVAudioSession.sharedInstance()
        
        if(paused)
        {
            audioRecorder.record()
            recording = true
        }
        else
        {
            do {
                try recordingSession.setCategory(.playAndRecord, mode: .default)
                try recordingSession.setActive(true)
            } catch {
                print("Failed to start recording session")
            }
        
            let documentPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        
            let audioFileName = documentPath.appendingPathComponent("\(Date().toString(dateFormat: "dd-MM-YY_'at'_HH:mm:ss")).wav")
        
            let settings = [AVFormatIDKey: Int(kAudioFormatLinearPCM), AVSampleRateKey: 44100, AVNumberOfChannelsKey: 1, AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue]
        
            do {
                audioRecorder = try AVAudioRecorder(url: audioFileName, settings: settings)
                audioRecorder.record()
                recording = true
            } catch {
                print("Could not start recording")
            }
        }
    }
    
    func stopRecording() {
        audioRecorder.stop()
        recording = false
        
        fetchRecordings()
    }
    
    func pauseRecording() {
        audioRecorder.pause()
        recording = false
    }
    
    func fetchRecordings () {
        recordings.removeAll()
        
        let fileManager = FileManager.default
        let documentDirectory = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let directoryContents = try! fileManager.contentsOfDirectory(at: documentDirectory, includingPropertiesForKeys: nil)
        
        for audio in directoryContents {
            
            let recording = Recording(fileURL: audio, createdAt: getCreationDate(for: audio))
            recordings.append(recording)
        }
        
        recordings.sort(by: {$0.createdAt.compare($1.createdAt) == .orderedDescending})
        objectWillChange.send(self)
    }
    
    func deleteRecording(urlsToDelete: [URL]) {
        
        for url in urlsToDelete {
            print(url)
            do {
                try FileManager.default.removeItem(at: url)
            } catch {
                print("File could not be deleted!")
            }
        }
        
        fetchRecordings()
    }
}
