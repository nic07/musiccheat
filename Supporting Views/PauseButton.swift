//
//  PauseButton.swift
//  MusicCheat
//
//  Created by Katutu on 11/6/19.
//  Copyright © 2019 Fulsto Engineering. All rights reserved.
//

import SwiftUI

struct PauseButton: View {
    var body: some View {
        GeometryReader { geometry in
            ZStack {
                Circle()
                    .foregroundColor(.red)
                    .frame(width: geometry.size.width, height: geometry.size.height)
                Circle()
                    .foregroundColor(.white)
                    .frame(width: geometry.size.width / 1.13, height: geometry.size.height / 1.13)
                HStack (alignment: .center) {
                    Capsule()
                        .frame(width: geometry.size.width / 8.5, height: geometry.size.width / 3.4)
                        .foregroundColor(.black)
                    Capsule()
                        .frame(width: geometry.size.width / 8.5, height: geometry.size.width / 3.4)
                        .foregroundColor(.black)
                }
            }
        }
    }
}

struct PauseButton_Previews: PreviewProvider {
    static var previews: some View {
        PauseButton()
    }
}
