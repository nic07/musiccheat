//
//  StopButton.swift
//  MusicCheat
//
//  Created by Katutu on 11/6/19.
//  Copyright © 2019 Fulsto Engineering. All rights reserved.
//

import SwiftUI

struct StopButton: View {
    var body: some View {
        GeometryReader { geometry in
            ZStack {
                Circle()
                    .foregroundColor(.red)
                    .frame(width: geometry.size.width, height: geometry.size.height)
                Circle()
                    .foregroundColor(.white)
                    .frame(width: geometry.size.width / 1.13, height: geometry.size.height / 1.13)
                Rectangle()
                    .foregroundColor(.black)
                    .frame(width: geometry.size.width < geometry.size.height ? geometry.size.width / 3.4 : geometry.size.height / 3.4, height: geometry.size.width < geometry.size.height ? geometry.size.width / 3.4 : geometry.size.height / 3.4)
                    .cornerRadius(geometry.size.width / 17)
            }
        }
    }
}

struct StopButton_Previews: PreviewProvider {
    static var previews: some View {
        StopButton()
    }
}
