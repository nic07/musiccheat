//
//  RecordButton.swift
//  MusicCheat
//
//  Created by Katutu on 11/6/19.
//  Copyright © 2019 Fulsto Engineering. All rights reserved.
//



import SwiftUI

struct RecordButton: View {
    var body: some View {
        GeometryReader { geometry in
        ZStack {
            Circle()
                .foregroundColor(.red)
                .frame(width: geometry.size.width, height: geometry.size.height)
            Circle()
                .foregroundColor(.white)
                .frame(width: geometry.size.width / 1.13, height: geometry.size.height / 1.13)
            Circle()
                .foregroundColor(.red)
                .frame(width: geometry.size.width / 1.36, height: geometry.size.height / 1.36)
            
            }
        }
    }
}

struct RecordButton_Previews: PreviewProvider {
    static var previews: some View {
        RecordButton()
    }
}
