//
//  Logo.swift
//  MusicCheat
//
//  Created by Katutu on 11/15/19.
//  Copyright © 2019 Fulsto Engineering. All rights reserved.
//

import SwiftUI



struct LogoWave: Shape {
    func path(in rect: CGRect) -> Path {
        let width = rect.width
        let height = rect.height
        
        let origin = CGPoint(x: 0, y: height * 0.4)
        
        var path = Path()
        path.move(to: origin)
        
        let step = Double( rect.width / 400)
        for angle in stride(from: step, through: 360, by: step) {
            let x = origin.x + CGFloat(angle/360) * width
            let y = origin.y
                - CGFloat(sin(angle/360 * 5 * Double.pi) * sin(angle / 360 * Double.pi)) * height/4
            path.addLine(to: CGPoint(x: x, y: y))
        }
        
        let y = origin.y - CGFloat(sin(0.75 * 5 * Double.pi) * sin(0.75 * Double.pi)) * height/4
        
        path.move(to: CGPoint(x: origin.x + 0.75 * width, y: y))
        path.addLine(to: CGPoint(x: origin.x + 0.75 * width, y: height / 1.2))
        
        let y2 = origin.y - CGFloat(sin(0.93 * 5 * Double.pi) * sin(0.93 * Double.pi)) * height/4
        path.move(to: CGPoint(x: origin.x + 0.93 * width, y: y2))
        path.addLine(to: CGPoint(x: origin.x + 0.93 * width, y: height / 1.6))
        
        return path
    }
    
}


struct Logo: View {
    var body: some View {
        GeometryReader { geometry in
            ZStack {
                Circle()
                    .foregroundColor(.red)
                    .frame(width: geometry.size.width, height: geometry.size.height)
                Circle()
                    .foregroundColor(.white)
                    .frame(width: geometry.size.width / 1.13, height: geometry.size.height / 1.13)
                LogoWave()
                    .stroke(Color.black, style: StrokeStyle(lineWidth: geometry.size.width / 20, lineCap: .round))
                    .frame(width: geometry.size.width / 1.23, height: geometry.size.height / 1.45)
                Circle()
                    .foregroundColor(.black)
                    .frame(width: geometry.size.height / 10, height: geometry.size.height / 10)
                    .offset(x: geometry.size.width/2 * 0.328, y: geometry.size.height * 0.22 )
                Circle()
                    .foregroundColor(.black)
                    .frame(width: geometry.size.height / 11, height: geometry.size.height / 11)
                    .offset(x: geometry.size.width/2 * 0.633, y: geometry.size.height * 0.077)
            }
        }
    }
}

struct Logo_Previews: PreviewProvider {
    static var previews: some View {
        Logo()
    }
}
