//
//  PlayButton.swift
//  MusicCheat
//
//  Created by Katutu on 11/6/19.
//  Copyright © 2019 Fulsto Engineering. All rights reserved.
//

import SwiftUI

struct PlayTriangle: Shape {
    func path(in rect: CGRect) -> Path {
        
        var path = Path()
        path.move(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x:0, y: rect.height))
        path.move(to: CGPoint(x:0, y: rect.height))
        path.addLine(to: CGPoint(x: CGFloat(rect.height * cos(.pi / 6)), y: rect.height / 2))
        path.move(to: CGPoint(x: CGFloat(rect.height * cos(.pi / 6)), y: rect.height / 2))
        path.addLine(to: CGPoint(x: 0, y: 0))
        //   path.closeSubpath()
        return path
    }
}

struct PlayButton: View {
    var body: some View {
        
        GeometryReader { geometry in
            
            ZStack {
                Circle()
                    .foregroundColor(.red)
                    .frame(width: geometry.size.width, height: geometry.size.height)
                Circle()
                    .foregroundColor(.white)
                    .frame(width: geometry.size.width / 1.13, height: geometry.size.height / 1.13)
                PlayTriangle()
                    .stroke(Color.black, style: StrokeStyle(lineWidth: geometry.size.width / 11.33, lineCap: .round))
                    .frame(width: geometry.size.width / 3.4, height: geometry.size.width / 3.4)
                    .padding(.leading, geometry.size.width / 8.5)
                
               // Circle()
                 //   .frame(width: geometry.size.width/5, height: geometry.size.height / 5)
               
            }
        }
    }
}

struct PlayButton_Previews: PreviewProvider {
    static var previews: some View {
        PlayButton()
    }
}
