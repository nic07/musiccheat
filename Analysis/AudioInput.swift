//
//  AudioInput.swift
//  MusicCheat
//
//  Created by Katutu on 11/16/19.
//  Copyright © 2019 Fulsto Engineering. All rights reserved.
//

import SwiftUI
import AVFoundation
import Accelerate


@objc class AudioInput: NSObject {

    var url: URL
    private var audioFile: AVAudioFile!
    //var outComplex : DSPSplitComplex
    public var fft : [Float]!
    
    init(url: URL)
    {
        
        self.url = url
        self.audioFile = try! AVAudioFile(forReading: self.url)
        let frameCount = UInt32(audioFile.length)
        print("FrameCount = \(frameCount)")
        
        guard let buffer = AVAudioPCMBuffer(pcmFormat: audioFile.processingFormat, frameCapacity: frameCount) else {
            print("Error getting the buffer did")
     //       super.init()
            return
        }
        
        do {
            try audioFile.read(into: buffer, frameCount: frameCount)
        }
        catch {
            print("caught an error")
        }
        
        //print("\nbuffer:\n")
        for k in 1...frameCount {
            
           // buffer.floatChannelData!.pointee[Int(k)]
            print( "value buffer \(buffer.floatChannelData!.pointee[Int(k)])")
        }
      
        let log2n = UInt(round(log2(Double(frameCount))))
        //print("log2n = \(log2n)")
        let bufferSizePOT = Int(1<<log2n)
        //print("bufferSizePot = \(bufferSizePOT)")
        
        let fftSetup = vDSP_create_fftsetup(log2n, Int32(kFFTRadix2))
        
        var realp = [Float] (repeating: 0.0, count: bufferSizePOT/2)
        var imagp = [Float] (repeating: 0.0, count: bufferSizePOT/2)

        var output = DSPSplitComplex(realp: &realp, imagp: &imagp)

        buffer.floatChannelData?.withMemoryRebound(to: DSPComplex.self, capacity: bufferSizePOT/2) {
            dspComplexStream in vDSP_ctoz(dspComplexStream, 2, &output, 1, UInt(bufferSizePOT/2))
        }
        
        // Perform fft here
        vDSP_fft_zrip(fftSetup!, &output, 1, log2n, Int32(FFTDirection(FFT_FORWARD)))

        print("\nspectrum:\n")
        print("\(realp.count)")
      /*  for i in 0...realp.count {
            if(!output.realp[i].isNaN)
            {
                print("\(i) \(output.realp[i])")
                //print("\(i): R = \(output.realp[i]) - i = \(output.imagp[i])")
        
            }
        }
        */
        var fft = [Float] (repeating: 0.0, count: Int(bufferSizePOT/2))
        let bufferOver2: vDSP_Length = vDSP_Length(bufferSizePOT/2)
        
//        //Scale the FFT data
//        vDSP_vsmul(output.realp, 1, &fftNormFactor, output.realp, 1, numberOfFramesOver2);
//        vDSP_vsmul(output.imagp, 1, &fftNormFactor, output.imagp, 1, numberOfFramesOver2);
//
        //Take the absolute value of the output to get in range of 0 to 1
        //   vDSP_zvabs(&output, 1, soundBuffer[inBusNumber].frequencyData, 1, numberOfFramesOver2);
       
        vDSP_zvmags(&output, 1, &fft, 1, bufferOver2)
        self.fft = fft
      //  outComplex = output
      for i in 0..<bufferSizePOT/2 {
        
        if(fft[i].isNaN)
            {
                //print("0")
            }
            else
            {
                print("\(fft[i])\n")
            }
        }
        print("done!");
        
        vDSP_destroy_fftsetup(fftSetup)
    }
    
    
    func getFFT()->[Float]
    {
        return self.fft
    }
    //let audioFile = try! AVAudioFile(forReading: url.lastPathComponent)
}



