//
//  TrackItem.swift
//  MusicCheat
//
//  Created by Katutu on 11/6/19.
//  Copyright © 2019 Fulsto Engineering. All rights reserved.
//

import SwiftUI
import AVFoundation

struct TrackItem: View {
    @ObservedObject var audioRecorder: AudioRecorder
    
    var body: some View {
        List {
            ForEach(audioRecorder.recordings, id: \.createdAt) { recording in
                 RecordingRow(audioURL: recording.fileURL)
             }.onDelete(perform: delete)
         }
    }

    
    func delete(at offsets: IndexSet) {
        var urlsToDelete = [URL]()
        for index in offsets {
            urlsToDelete.append(audioRecorder.recordings[index].fileURL)
        }
        audioRecorder.deleteRecording(urlsToDelete: urlsToDelete)
    }
    

    
}
struct RecordingRow: View {
    var audioURL: URL
    @ObservedObject var audioPlayer = AudioPlayer()
    
    var body: some View {
        HStack {
            Text("\(audioURL.lastPathComponent)")
                .font(.headline)
            Spacer()
            
            
            Text("\(getDuration(url: audioURL))")
            
            ZStack {
                Logo()
                .scaledToFit()
                NavigationLink(destination: FFT(url: audioURL)
                //.environmentObject(UserData())
            ) {
                Circle()
                    .hidden()
                    .frame(width: 0.0, height: 0.0, alignment: .trailing)
            }
            }.frame(width: 50, height: 50)
                .disabled(audioPlayer.isPlaying)
            
          
            if audioPlayer.isPlaying == false {
                Button(action: playAudio) {
                    PlayButton()
                        .scaledToFit()
                }.buttonStyle( PlainButtonStyle())
            } else {
                Button(action: stopAudio) {
                    StopButton()
                    .scaledToFit()
                }.buttonStyle(PlainButtonStyle())
            }
            
            
        }.frame(height: 50)
    }
    
    func playAudio () {
        audioPlayer.startPlayback(audio: self.audioURL)
    }

    
    func stopAudio () {
        audioPlayer.stopPlayback()
    }

    func getDuration(url: URL) -> String{
        let asset = AVAsset(url: url)
        let duration = asset.duration
        var seconds = CMTimeGetSeconds(duration).rounded()
        var hours = seconds / 3600
        hours = hours.rounded()
        seconds -= 3600 * hours
        var minutes = seconds / 60
        minutes = minutes.rounded()
        seconds -= 60 * minutes
        
        var hourString = String(Int(hours))
        if hours < 10 {
            hourString = "0" + hourString
        }
        
        var minuteString = String(Int(minutes))
        if minutes < 10 {
            minuteString = "0" + minuteString
        }
        
        var secondString = String(Int(seconds))
        if seconds < 10 {
            secondString = "0" + secondString
        }
        
        return hourString + ":" + minuteString + ":" + secondString
    }
    
    
}
struct TrackItem_Previews: PreviewProvider {
    static var previews: some View {
        TrackItem(audioRecorder: AudioRecorder())
    }
}
