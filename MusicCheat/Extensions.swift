//
//  Extensions.swift
//  MusicCheat
//
//  Created by Katutu on 11/14/19.
//  Copyright © 2019 Fulsto Engineering. All rights reserved.
//

import Foundation

extension Date
{
    func toString( dateFormat format  : String ) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }

}
