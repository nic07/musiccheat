//
//  AudioPlayer.swift
//  MusicCheat
//
//  Created by Katutu on 11/15/19.
//  Copyright © 2019 Fulsto Engineering. All rights reserved.
//

import SwiftUI
import Combine
import AVFoundation

class AudioPlayer: ObservableObject {

    let objectWillChange = PassthroughSubject<AudioPlayer, Never>()
    
    var isPlaying = false {
        didSet {
            objectWillChange.send(self)
        }
    }
}

