//
//  Home.swift
//  MusicCheat
//
//  Created by Katutu on 11/6/19.
//  Copyright © 2019 Fulsto Engineering. All rights reserved.
//

import SwiftUI
import AVFoundation
import os.log

var audioRecorder: AVAudioRecorder!
var recordingSession: AVAudioSession!
var audioPlayer: AVAudioPlayer!

var paused: Bool = false

struct Home: View {
    
    @ObservedObject var audioRecorder: AudioRecorder
    
    var body: some View {
        NavigationView {
     
                VStack {
                    // if recording == false {
                    HStack {
                        Button(action: pauseRecording) {
                            PauseButton()
                                .offset(x: audioRecorder.recording ? 0 :  UIScreen.main.bounds.width / 5, y: 0)
                                .opacity(audioRecorder.recording ? 1 : 0)
                                .scaledToFit()
                                .padding(.leading, 20)
                            
                        }.disabled(audioRecorder.recording ? false : true)
                        
                        Button(action: recordAudio) {
                            
                            RecordButton()
                                .opacity(audioRecorder.recording ? 0: 1)
                                .scaledToFit()
                        }.disabled(audioRecorder.recording ? true : false)
                            .onAppear() {
                                
                                //MARK: Setup for recorder
                                AVAudioSession.sharedInstance().requestRecordPermission { (hasPermission) in
                                    if hasPermission {
                                        print("ACCEPTED")
                                    }
                                }
                        }
                        
                        Button(action: stopRecording) {
                            StopButton()
                                .offset(x: audioRecorder.recording ? 0 : -200, y: 0)
                                .opacity(audioRecorder.recording ? 1 : 0)
                                .scaledToFit()
                                .padding(.trailing, 20)
                        }.disabled(audioRecorder.recording ? false : true)
                    }
                    
                    TrackItem(audioRecorder: AudioRecorder())
                }
                .navigationBarTitle("Tracks")
                .navigationBarItems(trailing: Text("Options"))
           
            //.navigationViewStyle(DefaultNavigationViewStyle())
            
        }.navigationViewStyle(StackNavigationViewStyle())
        
    }
    
    //MARK: Button Functionality
    func recordAudio() {
        withAnimation (.spring(response: 0.5, dampingFraction: 0.5, blendDuration: 2.0)) {
            audioRecorder.recording.toggle()
        }
        
        print ("paused = \(paused)")
        self.audioRecorder.startRecording(paused: paused)
    }
    
    func pauseRecording() {
        withAnimation {
            audioRecorder.recording.toggle()
        }
        
        self.audioRecorder.pauseRecording()
        paused = true
    }
    
    func stopRecording() {
        withAnimation {
            audioRecorder.recording.toggle()
        }
        
        self.audioRecorder.stopRecording()
        paused = false
    }
    
    // Function that gets part to directory
    func getDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectory = paths[0]
        
        return documentDirectory
    }
}
struct Home_Previews: PreviewProvider {
    static var previews: some View {
        Home(audioRecorder: AudioRecorder())
    }
}
